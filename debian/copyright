Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: utils
Source: https://github.com/juju/utils

Files: *
Copyright: 2011-2021 Canonical Ltd
           2014-2016 Cloudbase Solutions SRL
License: LGPL-3.0-with-exception

Files: du/*
Copyright: 2011 Rick Smith <magarace@yahoo.com>
License: public-domain

Files: filepath/stdlib.go filepath/stdlibmatch.go
Copyright: 2009-2010 The Go Authors
License: BSD-3-clause

Files: debian/*
Copyright:
 Copyright 2018-2021 Alexandre Viau <aviau@debian.org>
 Copyright 2021 Mathias Gibbens
License: LGPL-3.0-with-exception
Comment: Debian packaging is licensed under the same terms as upstream

License: public-domain
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org>

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the
 distribution.
    * Neither the name of Google Inc. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-3.0-with-exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; version 3 of the License.
 .
 On Debian systems, the complete text of the latest version of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 As a special exception to the GNU Lesser General Public License version 3
 ("LGPL3"), the copyright holders of this Library give you permission to
 convey to a third party a Combined Work that links statically or dynamically
 to this Library without providing any Minimal Corresponding Source or
 Minimal Application Code as set out in 4d or providing the installation
 information set out in section 4e, provided that you comply with the other
 provisions of LGPL3 and provided that you meet, for the Application the
 terms and conditions of the license(s) which apply to the Application.
 .
 Except as stated in this special exception, the provisions of LGPL3 will
 continue to comply in full to this Library. If you modify this Library, you
 may apply this exception to your version of this Library, but you are not
 obliged to do so. If you do not wish to do so, delete this exception
 statement from your version. This exception does not (and cannot) modify any
 license terms which apply to the Application, with which you must still
 comply.
